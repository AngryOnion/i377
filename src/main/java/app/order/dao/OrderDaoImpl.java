package app.order.dao;

//@Component
public class OrderDaoImpl {

//    @Autowired
//    private JdbcTemplate template;
//
//    private String insertQuery = "insert into orders (order_number) values (?)";
//    private String selectWhere = "select orders.id, orders.order_number, order_rows.item_name, order_rows.quantity, order_rows.price FROM orders LEFT JOIN order_rows ON id=order_id WHERE orders.id=?";
//    private String insertOrderRows = "insert into order_rows values (?, ?, ?, ?)";
//    private String selectJoin = "select orders.id, orders.order_number, order_rows.item_name, order_rows.quantity, order_rows.price FROM orders LEFT JOIN order_rows ON id=order_id";
//
//    @Override
//    public Order getById(long id) {
//        return template.query(selectWhere, new Object[]{id}, this::getOrder);
//    }
//
//    @Override
//    public Order saveOrder(Order order) {
//        GeneratedKeyHolder holder = new GeneratedKeyHolder();
//        template.update(con -> {
//            PreparedStatement preparedStatement = con.prepareStatement(insertQuery, new String[]{"id"});
//            preparedStatement.setString(1, order.getOrderNumber());
//            return preparedStatement;
//        }, holder);
//        order.setId(Objects.requireNonNull(holder.getKey()).longValue());
//        if (order.getOrderRows() != null) {
//            addOrderRows(order.getOrderRows(), order.getId());
//        }
//        return order;
//    }
//
//    private void addOrderRows(List<OrderRow> orderRows, long id) {
//        List<Object[]> batch = new ArrayList<>();
//        for (OrderRow row : orderRows) {
//            Object[] values = new Object[]{
//                    row.getItemName(),
//                    row.getQuantity(),
//                    row.getPrice(),
//                    id
//            };
//            batch.add(values);
//        }
//        template.batchUpdate(insertOrderRows, batch);
//    }
//
//    @Override
//    public void deleteOne(long id) {
//
//    }
//
//    @Override
//    public List<Order> getAllOrders() {
//        return template.query(selectJoin, this::getOrders);
//    }
//
//    private List<Order> getOrders(ResultSet rs) throws SQLException {
//        List<Order> orders = new ArrayList<>();
//        while (rs.next()) {
//            if (!orders.isEmpty() && orders.get(orders.size() - 1).getId() == rs.getLong("id")) {
//                Order order = orders.get(orders.size() - 1);
//                order.getOrderRows().add(new OrderRow(rs.getString(3), rs.getInt(4), rs.getInt(5)));
//            } else {
//                Order order = new Order();
//                order.setId(rs.getLong("id"));
//                order.setOrderNumber(rs.getString("order_number"));
//                order.setOrderRows(new ArrayList<>());
//                order.getOrderRows().add(new OrderRow(rs.getString(3), rs.getInt(4), rs.getInt(5)));
//                orders.add(order);
//            }
//        }
//        return orders;
//    }
//
//    private Order getOrder(ResultSet rs) throws SQLException {
//        Order order = new Order();
//        order.setOrderRows(new ArrayList<>());
//        while (rs.next()) {
//            order.setId(rs.getLong("id"));
//            order.setOrderNumber(rs.getString("order_number"));
//            order.getOrderRows().add(new OrderRow(rs.getString(3), rs.getInt(4), rs.getInt(5)));
//        }
//        return order;
//    }
}
