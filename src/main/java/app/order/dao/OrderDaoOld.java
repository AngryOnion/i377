package app.order.dao;

public class OrderDaoOld {

    private String insertQuery = "insert into orders (order_number) values (?)";
    private String deleteOrderRows = "delete from order_rows where order_id=?";
    private String deleteAllOrderRows = "delete from order_rows";
    private String getAllOrderRows = "select * from order_rows";
    private String deleteWhereQuery = "delete from orders where id=?";
    private String deleteAllOrders = "delete from orders";
    private String selectLast = "SELECT TOP 1 * FROM orders ORDER BY ID DESC";
    private String insertOrderRows = "insert into order_rows values (?, ?, ?, ?)";
    private String selectJoin = "select orders.id, orders.order_number, order_rows.item_name, order_rows.quantity, order_rows.price FROM orders LEFT JOIN order_rows ON id=order_id";

}
