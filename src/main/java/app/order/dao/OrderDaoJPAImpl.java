package app.order.dao;

import app.order.containers.Order;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional
public class OrderDaoJPAImpl implements OrderDao {

    @PersistenceContext
    EntityManager em;

    @Override
    @Transactional
    public Order getById(long id) {
        return em.find(Order.class, id);
    }

    @Transactional
    @Override
    public Order saveOrder(Order order) {
        em.persist(order);
        return order;
    }

    @Override
    @Transactional
    public void deleteOne(long id) {
        em.remove(getById(id));
    }

    @Override
    @Transactional
    public List<Order> getAllOrders() {
        Query query = em.createNamedQuery("Order.findAll");
        return (List<Order>) query.getResultList();
    }
}
