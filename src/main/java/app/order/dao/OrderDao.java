package app.order.dao;

import app.order.containers.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface OrderDao{

    @Transactional
    Order getById(long id);

    @Transactional
    Order saveOrder(Order order);

    void deleteOne(long id);

    List<Order> getAllOrders();
}
