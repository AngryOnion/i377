package app.order.service;

import app.order.containers.Order;
import app.order.dao.OrderDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderService {

    @Resource
    private OrderDao dao;

    public Order getById(long id) {
        return dao.getById(id);
    }

    public Order saveOrder(Order order) {
        return dao.saveOrder(order);
    }

    public void deleteOne(long id) {
        dao.deleteOne(id);
    }

    public List<Order> getAll() {
        return dao.getAllOrders();
    }
}
