package app.order.util;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Utility {


    public static Map<String, String> readSimpleJson(String json) {
        Map<String, String> result = new HashMap<>();
        Pattern pattern = Pattern.compile("\"(.+?)\":\\s*\"(.+?)\"");
        Arrays.stream(json.split(","))
                .forEach(string -> {
                    Matcher matcher = pattern.matcher(string);
                    while (matcher.find()) {
                        result.put(matcher.group(1), matcher.group(2));
                    }
                });
        return result;
    }

    public static String generateJsonFromOrder(Object object) {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{\n");
        Field[] declaredFields = object.getClass().getDeclaredFields();
        IntStream.range(0, declaredFields.length)
                .forEach(i -> {
                    jsonBuilder.append(runGetter(declaredFields[i], object));
                    if (i != declaredFields.length - 1) {
                        jsonBuilder.append(",\n");
                    }
                });
        jsonBuilder.append("\n}");
        return jsonBuilder.toString();
    }

    private static String runGetter(Field field, Object o) {
        StringBuilder jsonBuilder = new StringBuilder();
        Arrays.stream(o.getClass().getDeclaredMethods())
                .filter(method -> (method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3)))
                .filter(method -> method.getName().toLowerCase().endsWith(field.getName().toLowerCase()))
                .forEach(method -> {
                    try {
                        jsonBuilder.append(String.format("\"%s\": ", field.getName()));
                        jsonBuilder.append(String.format("\"%s\"", method.invoke(o)));
                    } catch (IllegalAccessException e) {
                        System.err.println("There is no access to such method: " + method.getName());
                    } catch (InvocationTargetException e) {
                        System.err.println("There is no such method: " + method.getName());
                    }
                });

        return jsonBuilder.toString();
    }

    public static String readStringFromInputStream(InputStream stream) throws IOException {
        String json;
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(stream))) {
            json = buffer.lines().collect(Collectors.joining("\n"));
        }
        return json;
    }

    public String getFileWithUtil(String fileName) {

        String result = "";

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
