package app.order.containers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationError {
    private String code;
    private List<String> arguments;



    @Override
    public String toString() {
        return "ValidationError{" +
                "code='" + code + '\'' +
                ", arguments=" + arguments +
                '}';
    }
}
