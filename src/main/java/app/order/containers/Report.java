package app.order.containers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Report {

    private Integer count = 0;
    private Integer averageOrderAmount = 0;
    private Integer turnoverWithoutVAT = 0;
    private Integer turnoverVAT = 0;
    private Integer turnoverWithVAT = 0;
}
