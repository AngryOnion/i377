package app.order.containers;

public enum ErrorType {
    TOO_SHORT("too_short_number");

    private String code;

    ErrorType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
