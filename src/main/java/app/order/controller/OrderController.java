package app.order.controller;

import app.order.containers.Order;
import app.order.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController("orders")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("orders/{id}")
    public @ResponseBody Order getOne(@PathVariable @Valid Long id) {
        Order order = orderService.getById(id);
        System.out.println(order);
        return order;
    }

    @PostMapping
    public Order order(@RequestBody @Valid Order order) {
        System.out.println(order);
        Order order1 = orderService.saveOrder(order);
        System.out.println(order1);
        return order1;
    }

//    @DeleteMapping("/{id}")
//    public void deleteOrder(@PathVariable Long id) {
//        orderService.deleteOne(id);
//    }
//
    @GetMapping
    public @ResponseBody List<Order> getAllOrders() {
        return orderService.getAll();
    }

}
